# kontist

Tech exercise for [Kontist](https://kontist.com/en) interview.


## Run

To run the server, once the dependencies are installed, you need to execute:

```
node src/index.js
```

## Tests

To run the tests, execute

```
yarn test
```

or 

```
npm test
```
