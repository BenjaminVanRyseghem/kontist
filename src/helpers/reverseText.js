/**
 * Reverse the provided TEXT.
 *
 * @param {string} text - Text to reverse
 * @return {string} Reversed text
 */

module.exports = function reverseText(text) {
	return text.split("")
		.reverse()
		.join("");
};
