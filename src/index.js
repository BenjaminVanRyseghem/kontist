const express = require("express");
const app = express();
const port = 3000;

const bodyParser = require("body-parser");
const multer = require("multer");
const upload = multer();

const reverseText = require("./helpers/reverseText");

app.use(bodyParser.urlencoded({ extended: true }));

app.post("/*", upload.array(), (req, res) => {
	let { text = "" } = req.body || {};
	let reversedText = reverseText(text);
	return res.send(reversedText);
});

app.listen(port, () => console.log(`Kontist app listening on port ${port}!`)); // eslint-disable-line no-console

module.exports = app;
