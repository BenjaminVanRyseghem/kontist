const reverseText = require("../../../src/helpers/reverseText");

describe("reverseText", () => {
	it("reverses text", () => {
		expect(reverseText("Hello")).toBe("olleH");
	});

	it("reverses empty text", () => {
		expect(reverseText("")).toBe("");
	});

	it("doesn't reverse undefined", () => {
		expect(reverseText).toThrowError();
	});
});
