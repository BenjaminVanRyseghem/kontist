const request = require("supertest");
const rewire = require("rewire");
const index = rewire("../../src/index");

describe("Server", () => {
	let reverseText = null;
	let revert = null;

	beforeEach(() => {
		reverseText = jasmine.createSpy("reverseText");
		revert = index.__set__("reverseText", reverseText); // eslint-disable-line no-underscore-dangle
	});

	afterEach(() => {
		revert();
	});

	it("accepts / POST requests", (done) => {
		request(index)
			.post("/")
			.expect(200)
			.end((err) => {
				if (err) {
					done.fail(err);
					return;
				}
				done();
			});
	});

	it("accepts /whatever POST requests", (done) => {
		request(index)
			.post("/whatever/foo/bar")
			.expect(200)
			.end((err) => {
				if (err) {
					done.fail(err);
					return;
				}
				done();
			});
	});

	it("accepts POST requests with text form param", (done) => {
		request(index)
			.post("/")
			.field("text", "Hello")
			.expect(200)
			.end((err) => {
				if (err) {
					done.fail(err);
					return;
				}
				done();
			});
	});

	it("reverses text form param", (done) => {
		request(index)
			.post("/")
			.field("text", "Hello")
			.end(() => {
				expect(reverseText).toHaveBeenCalledWith("Hello");
				done();
			});
	});
});
